import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';
import Header from './components/header';
import CriarQuarto from './pages/criarQuarto';
import QuartoListagem from './pages/quarto';

function App() {

  return (
    <div className="App">

      <div className="container">

        <BrowserRouter>

          <Header />

          <Route path="/" exact component={QuartoListagem} />
          <Route path="/quarto/criar" exact component={CriarQuarto} />

        </BrowserRouter>

      </div>

    </div>
  );
}

export default App;
