import './index.css'

function Header() {

    return (
        <div className="mt-4 d-flex align-items-center">
            <h4>Hotelaria</h4>
            <a className="mx-4 header-button">Reservas</a>
            <a className="mx-4 header-button" >Quartos</a>
            <a className="mx-4 header-button" >Hospedes</a>
        </div>
    )

}

export default Header;