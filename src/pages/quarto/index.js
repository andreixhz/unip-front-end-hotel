import axios from "axios";
import { useEffect, useState } from "react";

function QuartoItemLista({ quarto }) {

    return (
        <div className="color_item p-2 col-12 mb-2">
            Numero: {quarto.numero} Andar: {quarto.andar}
        </div>
    )

}

function QuartoListagem() {

    const [quartos, setQuartos] = useState([])

    useEffect(async function () {

        const res = await axios.get('https://f1e7-201-13-115-14.ngrok.io/quartos').then()
        setQuartos(res.data.objects)
        console.log(quartos)

    }, [])

    return (
        <div className="mt-4">
            <h1>Quartos</h1>

            <button className="button-primary mt-4">
                Criar
            </button>

            <div className="row m-0 mt-4">
                {quartos.map((quarto) => <QuartoItemLista quarto={quarto} />)}
            </div>

        </div>
    )

}

export default QuartoListagem;